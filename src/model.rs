//! Some things that are worth bringing up:
//!
//! The iffiest parse would definitely be `{.foo=bar}` — is that a key-value pair with `.foo` as
//! the key, or is it a class `foo=bar`? I would really rather just declare something like that
//! out-of-bounds.
//!
//! I would also prefer `{"foo=bar"}` to be rejected, or at least not parsed as a key-value pair,
//! and for `{.foo.bar}` to either be rejected, or parsed as two classes (like how CSS parses it).

use combine::error::ParseError;
use combine::parser::char::char;
use combine::stream::Stream;
use combine::{
    between, choice, eof, many, many1, one_of, optional, satisfy, sep_by1, sep_end_by1, EasyParser,
    Parser,
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) enum LangStringToken {
    LangToken(String),
    ClassAttribute(String),
    KeyValueAttribute(String, String),
}

pub(crate) fn parse_lang_string(s: &str) -> Result<(Vec<LangStringToken>, &str), String> {
    lang_string().easy_parse(s).map_err(|e| e.to_string())
}

pub(crate) fn lang_string<Input>() -> impl Parser<Input, Output = Vec<LangStringToken>>
where
    Input: Stream<Token = char>,
    // Necessary due to rust-lang/rust#24159
    Input::Error: ParseError<Input::Token, Input::Range, Input::Position>,
{
    fn is_alpha(c: char) -> bool {
        match c as u32 {
            0x41..=0x5a | 0x61..=0x7a => true,
            _ => false,
        }
    }
    fn is_digit(c: char) -> bool {
        match c as u32 {
            0x30..=0x39 => true,
            _ => false,
        }
    }
    fn is_bareword_char(c: char) -> bool {
        is_alpha(c) || is_digit(c) || c == '_' || c == '-' || c == ':'
    }
    fn is_nonquote_char(c: char) -> bool {
        c == '\t' || c == ' ' || c == '!' || ('\x23'..='\x7e').contains(&c)
    }

    let bareword = || {
        many1(satisfy(is_bareword_char).expected("<bareword>"))
            .map(|v: Vec<char>| String::from_iter(v))
    };

    let quoted_value = || {
        many(satisfy(is_nonquote_char).expected("<quoted value>"))
            .map(|v: Vec<char>| String::from_iter(v))
    };
    let quoted_string = || between(char('"'), char('"'), quoted_value());

    let token = || choice!(quoted_string(), bareword());

    let sep = || many1::<Vec<_>, _, _>(one_of(",\t ".chars()));

    let lang_token = || token().map(|tok| LangStringToken::LangToken(tok));

    let class_attribute = || (char('.'), token()).map(|(_, t)| LangStringToken::ClassAttribute(t));
    let key_value_attribute = || {
        (token(), char('='), token())
            .map(|(key, _, value)| LangStringToken::KeyValueAttribute(key, value))
    };
    let attribute = || choice!(class_attribute(), key_value_attribute());
    let attribute_list = || {
        sep_by1::<Vec<Option<LangStringToken>>, _, _, _>(optional(attribute()), sep())
            .map(|v| v.into_iter().flatten().collect::<Vec<LangStringToken>>())
    };
    let all_chars_except_closing_paren = || {
        many(satisfy(|c: char| c != ')').expected("<comment>"))
            .map(|v: Vec<char>| String::from_iter(v))
    };
    let comment = || between(char('('), char(')'), all_chars_except_closing_paren());

    let delimited_attribute_list = || between(char('{'), char('}'), attribute_list());
    let token_list = || sep_end_by1::<Vec<LangStringToken>, _, _, _>(lang_token(), sep());

    let skip_sep = || many1::<Vec<_>, _, _>(one_of(",\t ".chars())).silent();
    let lang_string = || {
        many(choice!(
            token_list(),
            delimited_attribute_list(),
            comment().map(|_| Vec::new()),
            skip_sep().map(|_| Vec::new())
        ))
        .map(|v: Vec<Vec<LangStringToken>>| {
            v.into_iter().flatten().collect::<Vec<LangStringToken>>()
        })
    };

    (lang_string(), eof()).map(|(a, _)| a)
}
