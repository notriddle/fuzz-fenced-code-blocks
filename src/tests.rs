use crate::model::{parse_lang_string, LangStringToken};
use LangStringToken::{ClassAttribute, KeyValueAttribute, LangToken};

#[test]
fn hardcoded_passing_tests() {
    fn do_check(xs: &str, expected: &[LangStringToken]) {
        let model_error;
        let (model_tokens, rem) = match parse_lang_string(xs) {
            Ok((model_tokens, rem)) => (model_tokens, rem),
            Err(err) => {
                model_error = format!("ERROR: {err}");
                (Vec::new(), &model_error[..])
            }
        };
        if rem != "" {
            panic!("{rem:#?}");
        }
        assert_eq!(expected, &model_tokens);
    }
    fn check(xs: &str, expected: &[LangStringToken]) {
        do_check(xs, expected);
        do_check(&format!(" {xs}"), expected);
        do_check(&format!("{xs} "), expected);
        do_check(&format!(" {xs} "), expected);
    }

    check("a", &[LangToken("a".into())]);
    check(r#""a""#, &[LangToken("a".into())]);
    check(r#""{.a}""#, &[LangToken("{.a}".into())]);

    check(
        "a{.b}",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a{ .b}",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a{.b }",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a{ .b }",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a {.b}",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a { .b}",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a {.b }",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );
    check(
        "a { .b }",
        &[LangToken("a".into()), ClassAttribute("b".into())],
    );

    check("{.b}", &[ClassAttribute("b".into())]);
    check("{ .b}", &[ClassAttribute("b".into())]);
    check("{.b }", &[ClassAttribute("b".into())]);
    check("{ .b }", &[ClassAttribute("b".into())]);

    check(
        "a{b=c}",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a{ b=c}",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a{b=c }",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a{ b=c }",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a {b=c}",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a { b=c}",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a {b=c }",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );
    check(
        "a { b=c }",
        &[
            LangToken("a".into()),
            KeyValueAttribute("b".into(), "c".into()),
        ],
    );

    check("{b=c}", &[KeyValueAttribute("b".into(), "c".into())]);
    check("{ b=c}", &[KeyValueAttribute("b".into(), "c".into())]);
    check("{b=c }", &[KeyValueAttribute("b".into(), "c".into())]);
    check("{ b=c }", &[KeyValueAttribute("b".into(), "c".into())]);

    check(r#"{"b"=c}"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{ "b"=c}"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{"b"=c }"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{ "b"=c }"#, &[KeyValueAttribute("b".into(), "c".into())]);

    check(r#"{b="c"}"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{ b="c"}"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{b="c" }"#, &[KeyValueAttribute("b".into(), "c".into())]);
    check(r#"{ b="c" }"#, &[KeyValueAttribute("b".into(), "c".into())]);

    check(r#"{b=""}"#, &[KeyValueAttribute("b".into(), "".into())]);
}

#[test]
fn hardcoded_failing_tests() {
    fn do_check(xs: &str) {
        let model_error;
        let (_, rem) = match parse_lang_string(xs) {
            Ok((model_tokens, rem)) => (model_tokens, rem),
            Err(err) => {
                model_error = format!("ERROR: {err}");
                (Vec::new(), &model_error[..])
            }
        };
        if rem == "" {
            panic!("failed to fail `{xs}`");
        }
    }
    fn check(xs: &str) {
        do_check(xs);
        do_check(&format!(" {xs}"));
        do_check(&format!("{xs} "));
        do_check(&format!(" {xs} "));
    }

    check("\"a");
    check(".a");
    check("{");
    check("a{");
    check("{a==b}");
    check("{a=b=c}");
    check(r#"{"a=b"}"#);
}
