use crate::{convert_token, model, rustdoc};
use quickcheck::TestResult;

#[quickcheck]
fn equiv(xs: String) -> TestResult {
    if !string_is_reasonable(&xs) {
        return TestResult::discard();
    }
    TestResult::from_bool(check(&xs).is_some())
}

fn string_is_reasonable(xs: &str) -> bool {
    xs.chars().all(|c|
        // printable characters, plus tab
        c == '\t' || ('\x20'..='\x7e').contains(&c))
}

fn check(xs: &str) -> Option<Vec<model::LangStringToken>> {
    let mut tag_iter = rustdoc::TagIterator::new(xs);
    let rustdoc_tokens = (&mut tag_iter).collect::<Vec<_>>();
    let model_error;
    let (model_tokens, rem) = match model::parse_lang_string(xs) {
        Ok((model_tokens, rem)) => (model_tokens, rem),
        Err(err) => {
            model_error = format!("ERROR: {err}");
            (Vec::new(), &model_error[..])
        }
    };
    let converted_rustdoc_tokens = rustdoc_tokens
        .into_iter()
        .map(convert_token)
        .collect::<Vec<model::LangStringToken>>();
    if rem != "" && tag_iter.errors.len() != 0 {
        // both parsers failed; this is fine
        Some(model_tokens)
    } else if converted_rustdoc_tokens != model_tokens {
        println!("{converted_rustdoc_tokens:#?} != {model_tokens:#?}");
        None
    } else if rem == "" && tag_iter.errors.len() != 0 {
        let e = tag_iter.errors.join(", ");
        println!("rustdoc parser errored, while model parser passed: {e}");
        None
    } else if rem != "" && tag_iter.errors.len() == 0 {
        println!("rustdoc parser passed, while model parser errored: {rem}");
        None
    } else {
        Some(model_tokens)
    }
}
